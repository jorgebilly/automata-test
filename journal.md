# Prueba Autómata



## Análisis

- Se ocupará django para realizar la captura de las encuetas 
- El primer reto es diseñar el modelado de la base de datos para soportar el almacenamiento de las encuestas
- El segundo reto mas grande es investigar como implementar las encuestas mediante los chatbox en las plataformas conocidas y elegir los 2 indicados


## Tecnologías

- Django
- Black
- Pytest
- Postgresql
- Python Telegram bot


## Pasos

- Crear el pryecto en PyCharm utilizando el enviroment Virtualenv
- Se configura el archivo requierements.txt para agregar todas las librerias que se ocuparan en el desarrollo
- A papel y lápiz se diseña el modelado de la base de datos para crear los modelos de django que se ocuparan 
- Se crean los modelos en Django 
- El admin de django se configura para poder crear la encuesta
- Se crean un par de registros para probar funcionalidad 
- Al investigar me doy cuenta que el boot de telegram y messenger y es fácil de implementar 
- Se configuran los archivos en django para ejecutar comandos y asi poder empezar a configurar el boot


## Preguntas

1. ¿Qué consideraciones hay que tener si quieres poner más canales de comunicación?
   - Entender la documentación de los n canales que se necesitan e implementar los métodos para la interacción con el usuario
   - Desarrollar las funciones para los bots de manera general a modo que solo se manden a llamar dependiendo de las acciones del usuario

2. ¿Qué arquitectura conviene si queremos atender a 10k usuarios concurrentes?
   - Ubuntu Server 22.04.1 LTS
   - Nginx 
   - Gunicorn
   - 8 Core 
   - 64 Gb RAM

3. ¿Qué servicios de infraestructura podríamos usar para minimizar los costos?
   - Se encuentra disponible Dialogflow para conservaciones con ayuda de IA de google, se puede implementar y con esto el alto costo que puede generar procesar todas las opciones de las encuestas disminuye en el servidor.

4. ¿Que propondrías usar si queremos tener nuestra propio chat? (App)
   - Ocuparía React Native para el desarrollo de la interfaz grafica para realizar un desarrollo sencillo y que se pueda utilizar en los diferentes dispositivos en el mercado
   - Se usaría Django rest framework y django channels para atender las peticiones del chat 
