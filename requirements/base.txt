Django==4.1.7
psycopg2-binary==2.9.5

requests
django-cors-headers==3.13.0
django-adminlte-3==0.1.6
python-telegram-bot>=20.1
