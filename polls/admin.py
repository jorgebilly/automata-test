from django.contrib import admin

# Register your models here.
from .models import Question, Option, User


# Viewing the questions
@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ("position", "question_text", "type", "status")


# Viewing the options
@admin.register(Option)
class OptionAdmin(admin.ModelAdmin):
    list_display = (
        "question",
        "option_text",
        "question_dependent",
    )


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ("first_name", "last_name", "origin", "date_add")
