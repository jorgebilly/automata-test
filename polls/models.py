from django.db import models


# Create your models here.

# Store the questions to be asked in the survey
class Question(models.Model):
    question_text = models.TextField()
    position = models.IntegerField(default=1)
    # label that will be displayed in the admin
    TYPE_CHOICE = ((1, "Abierta"), (2, "Opcion multiple"))
    type = models.IntegerField(choices=TYPE_CHOICE)
    # If a question is inactive, the chatbot will not send it
    status = models.BooleanField(default=True)

    # Display in the admin of the question
    def __str__(self):
        return self.question_text


class Option(models.Model):
    question = models.ForeignKey(
        Question, related_name="related_primary_question", on_delete=models.CASCADE
    )
    option_text = models.TextField()

    question_dependent = models.ForeignKey(
        Question,
        related_name="related_dependet_question",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    # Display in the admin of the option
    def __str__(self):
        return self.question.question_text + ": " + self.option_text


# The users who answer the surveys must be saved
class User(models.Model):
    id_app = models.CharField(max_length=20, default=None)
    first_name = models.CharField(max_length=100, default=None)
    last_name = models.CharField(max_length=100, default=None)
    # label that will be displayed in the admin
    ORIGIN_CHOICE = ((1, "Telegram"), (2, "Messenger"))
    origin = models.IntegerField(choices=ORIGIN_CHOICE)
    date_add = models.DateTimeField()


class Response(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    response_text = models.TextField()
    response_option = models.ForeignKey(Option, on_delete=models.CASCADE)
