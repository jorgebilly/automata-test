from asgiref.sync import sync_to_async
from polls.models import User
from datetime import datetime


# Get initial message
def init_conversation(first_name, last_name) -> str:
    return (
        "Hola "
        + first_name
        + " "
        + last_name
        + ", para iniciar la encuenta use la opcion /poll"
    )


# Register or modifi user
@sync_to_async
def register_user(id_app, first_name, last_name, origin) -> None:
    if not User.objects.filter(id_app=id_app, origin=origin).exists():
        user = User(
            id_app=id_app,
            first_name=first_name,
            last_name=last_name,
            origin=origin,
            date_add=datetime.now(),
        )
    else:
        user = User.objects.get(id_app=id_app, origin=origin)
        if user.first_name != first_name or user.last_name != last_name:
            user.first_name = first_name
            user.last_name = last_name
    user.save()
