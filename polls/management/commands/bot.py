from django.core.management.base import BaseCommand, CommandError

from polls.bot.bot_telegram import init_bot


class Command(BaseCommand):
    help = "Bot configuration"

    def handle(self, *args, **options):
        try:
            print("The bot starts")
            init_bot()
        except Exception as e:
            raise CommandError("Error starts bot: %s", e.__str__())
